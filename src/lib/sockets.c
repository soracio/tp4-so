#include <strings.h>
#include <errno.h>
#include "include/sockets.h"

int inet_connect(int sockfd, int port, int address){
	struct sockaddr_in servaddr; 
	bzero(&servaddr, sizeof(servaddr)); 

    //Assign IP, PORT 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = address; 
    servaddr.sin_port = htons(port);
  
    //Connect the client socket to server socket 
    if (connect(sockfd, (sockaddr_t*)&servaddr, sizeof(servaddr)) != 0) { 
        return 0;
    } 
    return 1;
}