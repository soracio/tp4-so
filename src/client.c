#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <strings.h>
#include <errno.h>
#include <unistd.h>

#include "lib/include/signals.h"
#include "lib/include/sockets.h"
#include "lib/include/getline.h"
#include "lib/include/errors.h"
#include "include/client.h"


#define NORMAL_TERMINATION 0

#define MESSAGE_FORMAT "%s\n"

#define CONNECTION_ERRCODE 2
#define CONNECTION_FAILED "Connection with the server failed..."
#define CONNECTION_SUCCESSFUL "Connection established!"

#define WRITE_ERRCODE 3
#define WRITE_FAILED "Write failed..."

#define INPUT_ERRCODE 4
#define INPUT_FAILED "Input could not be read..."

//Prototypes
static void on_termination();
static void on_kill();
static void prompt_user(int sockfd, char **line, size_t *linesize);

char *line = NULL;

//Functions
static void on_termination(){
    free(line);
}

static void on_kill(){
	on_termination();
    exit(NORMAL_TERMINATION);
}

static int end_prompting(char *line, ssize_t linelen){
    //If nothing was inputed end prompting
    if (linelen > 1) {
        return 0;
    }
    return 1;
}

static void prompt_user(int sockfd, char **line, size_t *linesize){
    //Reducing scope would mean redefining variable multiple times depending on compiler
    ssize_t linelen;
    int prompt;
    do {
        system("clear");

        //Read line
        linelen = getline(line, linesize, stdin);
        if (linelen == -1) {
            END_WITH_ERROR(INPUT_FAILED, INPUT_ERRCODE);
        }

        //Decide if should end prompting
        prompt = !end_prompting(*line, linelen);

        //If input is not end-of-input then write to server
        if(prompt && write(sockfd, *line, *linesize) == -1){
            END_WITH_ERROR(WRITE_FAILED, WRITE_ERRCODE);
        }

        //If not end-of-input prompt again
    } while(prompt);
}

int main()
{
	//Variable declarations
	sigaction_t action;
    int sockfd; 
    size_t linesize = 0;

    //Set on-kill to free allocated memory
    set_sigaction(&action, on_kill, SIGINT);
  
    //Socket create and verification 
    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        END_WITH_ERROR(SOCKET_FAILED, SOCKET_ERRCODE);
    } else {
        printf(MESSAGE_FORMAT, SOCKET_SUCCESSFUL); 
    }
    
    //Connection to server
    if (!inet_connect(sockfd, CLIENT_PORT, CLIENT_ADDRESS)){
        END_WITH_ERROR(CONNECTION_FAILED, CONNECTION_ERRCODE);
    } else {
        printf(MESSAGE_FORMAT, CONNECTION_SUCCESSFUL);
    }
    
    //Interact with server
    prompt_user(sockfd, &line, &linesize);

    //Resouce clean-up
    on_termination();

    return NORMAL_TERMINATION;
}

