#ifndef _ERRORS_H
#define _ERRORS_H

#define END_WITH_ERROR(__ERROR_MESSAGE, __EXIT_CODE) \
        perror(__ERROR_MESSAGE); \
        exit(__EXIT_CODE)

#endif