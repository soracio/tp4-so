#ifndef _SOCKETS_H
#define _SOCKETS_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

#define SOCKET_ERRCODE 1
#define SOCKET_FAILED "Socket creation failed..."
#define SOCKET_SUCCESSFUL "Socket successfully created.."

typedef struct sockaddr sockaddr_t;

int inet_connect(int sockfd, int port, int address);

#endif