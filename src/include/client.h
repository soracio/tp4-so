#ifndef _CLIENT_H
#define _CLIENT_H

#include "server.h"

#define CLIENT_PORT SERVER_PORT
#define CLIENT_ADDRESS inet_addr("127.0.0.1")

#endif