#include "include/signals.h"
#include <string.h>

int set_sigaction(SigAction action, void(*sa_handler)(int), int signum){
	if (action == NULL){
		return 0;
	}
	memset(action, 0, sizeof(*action));
	action->sa_handler = sa_handler;
	sigaction(signum, action, NULL);
	return 1;
}