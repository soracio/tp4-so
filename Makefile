CC = gcc
CFLAGS = -D_POSIX_C_SOURCE -std=c99 -Wall -Wextra
DEPS = $(src_include)/client.h $(lib_include)/errors.h $(lib_include)/signals.h \
$(lib_include)/sockets.h $(src_include)/server.h $(lib_include)/getline.h \
$(lib_include)/getdelim.h
LDFLAGS = 

src = src
lib = $(src)/lib
src_include = $(src)/include
lib_include = $(lib)/include
obj = client server

client_src = $(src)/client.c $(lib)/signals.c $(lib)/sockets.c $(lib)/getline.c \
$(lib)/getdelim.c

server_src = $(src)/server.c

client: $(client_src)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

server: $(server_src)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -c -o $@ $^ $(LDFLAGS)


.PHONY: clean
clean:
	rm -f $(obj)

.PHONY: all
all: $(obj)