// Server side C/C++ program to demonstrate Socket programming 

#include <unistd.h> 
#include <time.h>
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 

#include "lib/include/errors.h"
#include "lib/include/sockets.h"
#include "lib/include/getline.h"


#define PORT 23423

#define WHAT0 0
#define WHAT1 12345

#define clear() system("clear")
#define MAX_LEVEL 11
#define LEVEL_3_ANSWER "la respuesta a este acertijo es cabeza de calabaza"
#define LEVEL_6_ANSWER "la respuesta a este acertijo es indeterminado"
#define LEVEL_8_ANSWER "La respuesta a este acertijo es cachiporra"
#define LEVEL_9_ANSWER "La respuesta a este acertijo es gdb rules"
#define RUNME ".runme\n"

#define SOCKOPT_ERRCODE 2
#define SOCKOPT_FAILED "Setsockopt failed..."

#define SOCKOPT_LEVEL SOL_SOCKET
#define SOCKOPT_OPTNAME SO_REUSEADDR | SO_REUSEPORT
#define SOCKOPT_OPTVAL 1

#define BIND_ERRCODE 3
#define BIND_FAILED "Bind failed..."

#define LISTEN_ERRCODE 4
#define LISTEN_FAILED "Could not mark as pasive socket..."

#define ACCEPT_ERRCODE 5
#define ACCEPT_FAILED "Conection with client unsuccessful..."

#define CLI_DISCONECTED_ERRCODE 6
#define CLI_DISCONECTED_MSG "Client disconected"

#define ALLOWED_CLIENTS 1

#define CHALLENGE_BANNER "------------- DESAFÍO %d -------------\n"
#define QUESTION_BANNER "PREGUNTA PARA INVESTIGAR:\n%s\n"
#define GREATEST_SECRET "Shhhhhh... IT'S A SECRET TO EVERYBODY! Run:\n\nreadelf -S server | grep -o \"\\.[^ ]\\+\" | grep -B 3 -A 3 \".runme\"\n;) ;)\n"
#define TRY_AGAIN "try again\n"
#define CONGRATS "=================¡Felicidades!=================\n===================¡Ganaste!===================\n\n"
#define SCRAMBLE_MSG_MAX_MSG_LENGTH 51
#define SCRAMBLE_MSG_MSG_COUNT 4
#define SCRAMBLE_MSG_WRITE 1

#define ANSWER_BUFFER_SIZE 100
#define RIGHT_ANSWER "Respuesta correcta.\n"
#define WRONG_ANSWER "Respuesta incorrecta: %s\n"
#define ANSWER_SUBMITION_DELAY 2

#define GREET "¡Genial!"
#define QUINE_COMPILE "gcc quine.c -o quine"
#define QUINE_COMPILED GREET ", ya lograron meter un programa en quine.c, veamos si hace lo que corresponde:\n"
#define QUINE_DIFF "./quine | diff ./quine.c -"
#define QUINE_DIFF_FALSE "\ndiff returned: %d\n"
#define QUINE_DIFF_TRUE GREET LEVEL_8_ANSWER "\n"

#define LINE_SEPARATOR "\n\n"

char parts[7][10] = {".data", ".bss", ".comment", RUNME, ".shstrtab", ".symtab", ".strtab"};
char answers[MAX_LEVEL][50] = {"entendido\n", "#0854780*\n", "nokia\n", "cabeza de calabaza\n", "easter_egg\n", RUNME, "indeterminado\n", "this is awesome\n", "cachiporra\n", "gdb rules\n", "/lib/x86_64-linux-gnu/ld-2.19.so\n"};
char riddles[MAX_LEVEL][2048] = {"Hola Ariel! Felicidades por resolver el primer acertijo!\nDeberías estar familiarizado con este server, así que solo necesitás escribir \"entendido\\n\"\n",
                                    "# \\033[D \\033[A \\033[A \\033[D \\033[B \\033[C \\033[B \\033[D *\n",
                                    "https://vocaroo.com/i/s19015zmR4t8\n",
                                    "EBADF... abrilo y verás\n",
                                    "respuesta = strings[215]\n",
                                    ".init .plt .text ? .fini .rodata .eh_frame_hdr\n",
                                    "mixed fds\n",
                                    "Tango Hotel India Sierra India Sierra Alfa Whiskey Echo Sierra Oscar Mike Echo\n",
                                    "quine\n",
                                    "b gdbme y encontrá el valor mágico\n",
                                    "/lib/x86_64-linux-gnu/libc-2.19.so ?\n"};

char questions[MAX_LEVEL][1024] = {"¿Cómo descubrieron el protocolo, la dirección y el puerto para conectarse?\n",
                                    "¿Qué diferencias hay entre TCP y UDP y en qué casos conviene usar cada uno?\n",
                                    "¿El puerto que usaron para conectarse al server es el mismo que usan para mandar las respuestas? ¿Por qué?\n",
                                    "¿Qué útil abstracción es utilizada para comunicarse con sockets? ¿se puede utilizar read(2) y write(2) para operar?\n",
                                    "¿Cómo garantiza TCP que los paquetes llegan en orden y no se pierden?\n",
                                    "Un servidor suele crear un nuevo proceso o thread para atender las conexiones entrantes. ¿Qué conviene más?\n",
                                    "¿Cómo se puede implementar un servidor que atienda muchas conexiones sin usar procesos ni threads?\n",
                                    "¿Qué aplicaciones se pueden utilizar para ver el tráfico por la red?\n",
                                    "Sockets es un mecanismo de IPC. ¿Qué es más eficiente entre sockets y pipes?\n",
                                    "¿Cuáles son las características del protocolo SCTP?\n",
                                    "¿Qué es un RFC?\n"};

void tell_riddle(int level)
{
    clear();
    if(level > 0)
        printf(CHALLENGE_BANNER, level);
    printf("%s\n", riddles[level]);
}

void ask_question(int level)
{
    printf(QUESTION_BANNER, questions[level]);
}

int set_server_socket(int allowed_clients)
{
    // Fuente: https://www.geeksforgeeks.org/socket-programming-cc/
    int server_fd;
    struct sockaddr_in server_address;
    int opt = SOCKOPT_OPTVAL;

    // Creating socket file descriptor 
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        END_WITH_ERROR(SOCKET_FAILED, SOCKET_ERRCODE);
    } 
       
    // Forcefully attaching socket to the port 
    if (setsockopt(server_fd, SOCKOPT_LEVEL, SOCKOPT_OPTNAME, &opt, sizeof(opt)))
    { 
        END_WITH_ERROR(SOCKOPT_FAILED, SOCKOPT_ERRCODE);
    } 

    server_address.sin_family = AF_INET; 
    server_address.sin_addr.s_addr = INADDR_ANY; 
    server_address.sin_port = htons(PORT); 
       
    // Forcefully attaching socket to the port 
    if (bind(server_fd, (struct sockaddr *)&server_address, sizeof(server_address)) < 0) 
    { 
        END_WITH_ERROR(BIND_FAILED, BIND_ERRCODE);
    }

    // Set server to listen to messages
    if (listen(server_fd, allowed_clients) < 0) 
    { 
        END_WITH_ERROR(LISTEN_FAILED, LISTEN_ERRCODE);
    }

    return server_fd;
}

void __attribute__((section(".runme"))) runme() {
    printf(GREATEST_SECRET);
}

void ebadf()
{
    write(5, LEVEL_3_ANSWER, sizeof(LEVEL_3_ANSWER));
}

void gdbme()
{
    if(WHAT0 == WHAT1)
        printf(LEVEL_9_ANSWER "\n");
    else
        printf(TRY_AGAIN);
}

void scramble_message()
{
    srand(time(NULL));
    char strings[SCRAMBLE_MSG_MSG_COUNT][SCRAMBLE_MSG_MAX_MSG_LENGTH] = {LEVEL_6_ANSWER, "lapizzadedoncangrejoeslapizzaparatiyparami", "waluiginumberonekilogramodecalabazahacericasopa", "shrekisloveshrekislifearielparaqueleesesto"};
    int chars_left[SCRAMBLE_MSG_MSG_COUNT] = {0};

    for (int i = 0; i < SCRAMBLE_MSG_MSG_COUNT; ++i)
    {
        chars_left[i] = strlen(strings[i]);
    }
    int strings_finished = 0;
    int drawn_string, output_fd;
    while(strings_finished < SCRAMBLE_MSG_MSG_COUNT)
    {
        drawn_string = rand() % SCRAMBLE_MSG_MSG_COUNT;
        if(chars_left[drawn_string] > 0)
        {
            int index = strlen(strings[drawn_string]) - chars_left[drawn_string];
            if(drawn_string == 0)
            	output_fd = STDOUT_FILENO; // La respuesta se imprime por stdout
            else
            	output_fd = STDERR_FILENO; // Los otros strings salen por stderr mezclados
            write(output_fd, strings[drawn_string] + index, SCRAMBLE_MSG_WRITE);
            chars_left[drawn_string]--;
            if(chars_left[drawn_string] == 0)
                strings_finished++;
        }
    }
    printf(LINE_SEPARATOR);
}

void quine(){
    if(system(QUINE_COMPILE) == 0)
    {
        printf(QUINE_COMPILED);
        int diff_value = system(QUINE_DIFF);
        if(diff_value == 0)
            printf(QUINE_DIFF_TRUE);
        else
            printf(QUINE_DIFF_FALSE, diff_value);
    }
    printf(LINE_SEPARATOR);
}

void play(int socket, int server_fd)
{
    int valread;
    int level = 0;
    int answer_submitted = 1;
    char answer[ANSWER_BUFFER_SIZE];
    //size_t n = 0;
    int index = 0;
    //ssize_t answerlen;
    while(level < MAX_LEVEL)
    {
        if(answer_submitted){
            tell_riddle(level);
            switch(level)
            {
                case 3:
                        ebadf();
                        break;
                case 6:
                        scramble_message();
                        break;
                case 8: 
                        quine();
                        break;
                case 9:
                        gdbme();
                        break;
            }
            ask_question(level);
            answer_submitted = 0;
        }        
        valread = read(socket, answer+index, 1);
        if(valread == 0)
        {
            close(server_fd);
            END_WITH_ERROR(CLI_DISCONECTED_MSG, CLI_DISCONECTED_ERRCODE);
        }

        if(answer[index] == '\n')
        {
            answer_submitted = 1;
            answer[index + 1] = 0;
            if(strcmp(answer, answers[level]) != 0)
            {
                printf(WRONG_ANSWER, answer);
                sleep(ANSWER_SUBMITION_DELAY);
            }
            else
            {
                printf(RIGHT_ANSWER);
                sleep(ANSWER_SUBMITION_DELAY);
                level++;
            }
            index = 0;
        }
        else if(answer[index] == 0){
            index = 0;
            for (int i = 0; i < ANSWER_BUFFER_SIZE; ++i)
            {
                answer[i] =0;
            }
        }
        else
            index++;
        
    }
    clear();
    printf(CONGRATS);
}

int main() 
{ 
    int server_fd, new_socket;
    struct sockaddr_in client_address;
    int addrlen = sizeof(client_address);

    server_fd = set_server_socket(ALLOWED_CLIENTS);

    if ((new_socket = accept(server_fd, (struct sockaddr *)&client_address, (socklen_t*)&addrlen)) < 0)
    {
        END_WITH_ERROR(ACCEPT_FAILED, ACCEPT_ERRCODE);
    }

    // Connection established, now we have our own code
    play(new_socket, server_fd);

    // Game has ended
    close(new_socket);
    close(server_fd);
    return 0;
}


char bonus[181][51] ={"Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Argentina", "Armenia", "Australia", "Austria",
                    "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Belarus", "Belgium", "Belize", "Benin", "Bhutan", "Bolivia",
                    "Bosnia", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada",
                    "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Costa Rica", "Croatia", "Cuba",
                    "Cyprus", "Czechia", "Denmark", "Djibouti", "Dominica", "East Timor", "Ecuador", "Egypt", "El Salvador", "England", "Eritrea",
                    "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece",
                    "Guatemala", "Guinea", "Guyana", "Haiti", "Honduras", "Hungary", "Iceland", "India", "Indonesia",
                    "Iran", "Iraq", "Ireland", "Israel", "Italy", "Ivory Coast", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea",
                    "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
                    "Macedonia", "Madagascar", "Malawi", "Malaysia", "Mali", "Malta", "easter_egg", "Mauritania", "Mauritius", "Mexico",
                    "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand",
                    "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland",
                    "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", "Saudi Arabia", "Scotland", "Senegal",
                    "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Somalia", "South Africa", "South Sudan", "Spain",
                    "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand",
                    "Togo", "Tonga", "Tunisia", "Turkey", "Turkmenistan", "Uganda", "Ukraine", "United Kingdom", "United States", "Uruguay", "Uzbekistan",
                    "Vanuatu", "Vatican", "Venezuela", "Vietnam", "Wales", "Yemen", "Zambia", "Zimbabwe"};