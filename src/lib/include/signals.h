#ifndef _SIGNALS_H
#define _SIGNALS_H

#include <signal.h>

typedef struct sigaction sigaction_t;
typedef sigaction_t *SigAction;
int set_sigaction(SigAction action, void(*sa_handler)(int), int signum);

#endif